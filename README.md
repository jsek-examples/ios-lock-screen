# iOS lock screen demo with pure CSS

Inspired by [Rebuilding iOS 15 with Tailwind CSS
](https://youtu.be/eSzNNYk7nVU), but built with pure CSS on Vue.js 3.x

## Run locally

```sh
> yarn
> yarn dev
```

![](screenshot.png)